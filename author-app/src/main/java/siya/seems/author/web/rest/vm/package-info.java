/**
 * View Models used by Spring MVC REST controllers.
 */
package siya.seems.author.web.rest.vm;
