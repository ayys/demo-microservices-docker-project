all: author todo

docker-login:
	docker login

author: docker-login
	echo "Building Docker image for Author Service"
	cd author-app
	docker build -t ayys/demo-author-app:latest .
	cd ..

todo: docker-login
	echo "Building Docker image for TODO Service"
	cd todo-app
	docker build -t ayys/demo-todo-app:latest .
	cd ..

run:
	docker-compose up

push:
	docker push ayys/demo-todo-app:latest
	docker push ayys/demo-author-app:latest
