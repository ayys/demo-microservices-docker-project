from flask import Flask
from redis import Redis, RedisError
import os
import socket
from threading import Thread
from peewee import *



# Connect to Redis
redis = Redis(host="redis", db=0, socket_connect_timeout=2, socket_timeout=2)

# create a pub/sub object for redis
pubsub = redis.pubsub()

# connect to sqlite db
db = SqliteDatabase("todo.db")

app = Flask(__name__)

class Todo(Model):
    content = TextField()

    class Meta:
        database = db
        table_name = "todo"     # optional

class Author(Model):
    name = TextField()
    class Meta:
        database = db
        table_name = "author"
        
db.connect()
db.create_tables([Todo, Author])

def author_message_handler(message):
    if message and message["type"] == "message":
        import json
        data = message["data"].decode('utf-8')
        obj = json.loads(data)
        Author(name = obj['name']).save()
        print(data)

def store_author_to_database():
    import time
    pubsub.subscribe(**{"author": author_message_handler})
    while True:
        pubsub.get_message()
        time.sleep(0.1)

@app.route("/")
def hello():
    try:
        visits = redis.incr("counter")
    except RedisError:
        visits = "<i>cannot connect to Redis, counter disabled</i>"
    html = '''
    <h3>Authors</h3>
    <b>Hostname:</b> {hostname}<br/>
'''
    for author in Author.select():
        html += "<b>Name: <b>{author_name}<br>".format(author_name = author.name)
    return html.format(name=os.getenv("NAME", "world"), hostname=socket.gethostname(), visits=visits)

if __name__ == "__main__":
    author_thread = Thread(target = store_author_to_database)
    author_thread.start()
    app.run(host='0.0.0.0', port=4000)
